import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SushiOrderSystem2 {
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JButton cancelButton;
    private JButton checkoutButton;
    private JButton magurobutton;
    private JButton ikurabutton;
    private JButton salmonbutton;
    private JButton unibutton;
    private JButton hotatebutton;
    private JButton tunabutton;
    private JPanel root;
    int sum = 0;


    void order(String sushiname, int price) {
        String[] plateOptions = {"×1", "×2", "×3", "×4"};
        String[] dialogOptions = {"OK", "Cancel"};

        JPanel panel = new JPanel();
        panel.add(new JLabel("Please select number of plates"));
        JComboBox<String> plateComboBox = new JComboBox<>(plateOptions);
        panel.add(plateComboBox);

        int result = JOptionPane.showOptionDialog(
                null,
                panel,
                "Plate",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                null,
                dialogOptions,
                dialogOptions[0]
        );

        if (result == JOptionPane.OK_OPTION) {
            String selectedPlate = (String) plateComboBox.getSelectedItem();
            int plateCount = 0;
            switch (selectedPlate) {
                case "×1":
                    plateCount = 1;
                    break;
                case "×2":
                    plateCount = 2;
                    break;
                case "×3":
                    plateCount = 3;
                    break;
                case "×4":
                    plateCount = 4;
                    break;
            }

            int confirmation = JOptionPane.showOptionDialog(
                    null,
                    "Would you like to order " + sushiname + "?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    dialogOptions,
                    dialogOptions[0]
            );

            if (confirmation == JOptionPane.YES_OPTION) {
                String currentText = textArea1.getText();
                JOptionPane.showMessageDialog(
                        null,
                        "Order for " + sushiname + " received.",
                        "Order Received",
                        JOptionPane.INFORMATION_MESSAGE
                        );
                textArea1.setText(currentText + sushiname + " ×" + plateCount + " " + (price * plateCount) + " JPY\n");
                sum += price * plateCount;
                textArea2.setText("Total: " + sum + " JPY");
            }
        }
    }

    public SushiOrderSystem2() {
        textArea1.setText("Ordered Menu\n");
        textArea2.setText("Total: " + sum + " JPY");

        magurobutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("maguro", 120);
            }
        });
        magurobutton.setIcon(new ImageIcon(
                this.getClass().getResource("maguro.jpg")
        ));

        ikurabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ikura", 200);
            }
        });
        ikurabutton.setIcon(new ImageIcon(
                this.getClass().getResource("ikura.jpg")
        ));

        salmonbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("salmon", 130);
            }
        });
        salmonbutton.setIcon(new ImageIcon(
                this.getClass().getResource("salmon.jpg")
        ));

        unibutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("uni", 190);
            }
        });
        unibutton.setIcon(new ImageIcon(
                this.getClass().getResource("uni.jpg")
        ));

        hotatebutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("hotate", 150);
            }
        });
        hotatebutton.setIcon(new ImageIcon(
                this.getClass().getResource("hotate.jpg")
        ));

        tunabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tuna", 110);
            }
        });
        tunabutton.setIcon(new ImageIcon(
                this.getClass().getResource("tuna.jpg")
        ));

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int response = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to pay the bill?",
                        "Check Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (response == JOptionPane.YES_OPTION) {
                    JOptionPane.showMessageDialog(
                            null,
                            "The bill is " + sum + " yen.",
                            "Check",
                            JOptionPane.INFORMATION_MESSAGE
                    );

                    textArea1.setText("Ordered Menu\n");
                    sum = 0;
                    textArea2.setText("Total: " + sum + " JPY");
                }
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int response = JOptionPane.showConfirmDialog(
                        null,
                        "Are you sure you want to cancel?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (response == JOptionPane.YES_OPTION) {
                    textArea1.setText("Ordered Menu\n");
                    sum = 0;
                    textArea2.setText("Total: " + sum + " JPY");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SushiOrderSystem2");
        frame.setContentPane(new SushiOrderSystem2().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}



